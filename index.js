let countriesList = "";
let timezone="";
let insertdatafromAPI=document.getElementById('box');
function startLoadingAPI()
{
   
    fetch('https://restcountries.com/v3.1/all').then((apidata)=>{
        console.log(apidata);
        return apidata.json();
})
.then((actualdata)=>{
    console.log(actualdata);
    countriesList=actualdata;
    DisplayCountries(countriesList);

})
.catch((err)=>{
    console.log("Error is : ${err}"+err);

})
}
function DisplayCountries(countries){
    let str="";
    

    for( let country in countries){
        if(countries[country].timezones!=undefined){
            var currentTime=new Date($.now());
            timezone = countries[country].timezones[0].substring(4).split(':');
            timezone = (parseInt(timezone[0])*60+parseInt(timezone[1]))*60000;
            timezone = new Date(currentTime.getTime()+timezone-19800000);
            timezone= timezone.toLocaleString();
            // console.log(timezone);
        }

        let currencyList="";
        if(countries[country].currencies != undefined){
            for(let currency in countries[country].currencies){
                currencyList+=(" "+countries[country].currencies[currency].name);
            }
        }
        else{currencyList="No Currency"}

        let str1 = `<div class="row my-1 mx-1  border border-dark rounded">
            <div class="col-4 my-1 country-img">
                <img src="${countries[country].flags.svg}" class=" col-12 " >
            </div>
            <div class="col-8 mt-1 country-info">
                <h5>${countries[country].name.official}</h5>
                <p class="my-0">Currency: ${currencyList}</p>
                <p class="my-0">Current Date and Time: ${timezone}  </p>
                <button type="button" class="btn col-6 show-map btn-outline-primary"> <a href="${countries[country].maps.googleMaps}" target="_blank">Show Map</a></button>
                <button type="button" class="btn col-5 country-details btn-outline-primary"><a href="detail.html?country=${countries[country].name.common}" >Details</a></button>
            
            </div>
        </div>
            `; 
            str+=str1;
    }
    insertdatafromAPI.innerHTML=str;

}
function search(){
    let input=document.getElementById('search1').value.toUpperCase();
    console.log(input);
    let resultdata=countriesList.filter((country)=>{
        return country.name.official.toUpperCase().includes(input);
    });
    DisplayCountries(resultdata);
    console.log(resultdata);
    console.clear;
    
    
}
